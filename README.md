# First things first#
To make your life easier, if you haven't already, it is recommended you have the following in `$HOME/.asetup`:
```
[defaults]
briefprint      = True
autorestore     = True
```
This will allow you to ommit using "here" when setting up `Athena`, and will eliminate the need to specify the release each time (i.e. calling `asetup` will read the `.asetup.save` in your working directory and fall back to `$HOME/.asetup.save` if there isn't one).
# Setup on `lxplus` or other `cvmfs`-equipped node#
```
$ mkdir build source run && cd build
$ setupATLAS && acmSetup --sourcedir=../source 21.2.42,AnalysisBase
```
Subsequent setups require only:
```
$ cd build && setupATLAS && acmSetup
```
without the `acmSetup` arguments (unless you are changing releases).

**Experimental**: If you want to run the latest (and greatest) nightly, use:
```
$ asetup 21.2,AnalysisBase,latest
```
but **N.B.** that things *may not work*! If not, please send a mail to the analysis mailing list!
# Get the code #
```
$ acm clone_project DijetFramework dijetpluslepton/DijetFramework
```
This will clone and checkout the `master` branch. If you are just running the code, it's fine to stay on `master`. However, if you're going to be developing or making changes to the code, you'll need to create your own branch (since we don't want people modifying `master` directly, but instead through merge requests). Create your own branch based on `master`, e.g.:
```
$ cd DijetFramework
$ git checkout -b vpascuzz-dev origin/master
```
and submit merge requests (MR) once you have validated the changes.

# Compilation #
First, add the project packages you want to compile
```
$ acm add_pkg DijetFramework/xAODAnaHelpers DijetFramework/DijetResonanceAlgo
```
then compile:
```
$ acm compile
```

# Running the code #
## Local tests ##
For *local* testing, you will need to specify the input file to use. For `data16`:
```
$ INPUT=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/data16r21/data16_13TeV.00301912.physics_Main.deriv.DAOD_JETM2.r9264_p3083_p3213/DAOD_JETM2.11570376._000001.pool.root.1
```
For `MC16`:
```
$INPUT=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/mc16r21/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_JETM2.e3601_s3126_r9781_r9778_p3263/DAOD_JETM2.12273053._000001.pool.root.1
```

and run:
```
$ cd ../run
$ xAH_run.py --files $INPUT \
--config $WorkDir_DIR/data/DijetResonanceAlgo/config_mc16a_background.py \
-f direct
```

If you need to run over many local files, then use `--inputList` option. Here is an example running over all data DAOD files:
```
ls -d -1 [directory with DAOD]/* > data.in
xAH_run.py --files data.in --inputList --config $WorkDir_DIR/data/DijetResonanceAlgo/[config].py -f direct
``` 

## GRID submission ##
Once you're setup:
```
$ lsetup rucio
$ localSetupPandaClient
$ python $WorkDir_DIR/bin/runGridSubmission.py [runOn]
```
where `runOn` is one of:
```
data15repro, data15, data16, data17, data16extra, data16debug, qcdpythia, qcdsherpa, qcdpowpythia, qcdherwig, qcdpowherwig, wzprime, tc, ttbar, wpjets
```

## Batch (`condor`) submission ##
Some modifications to the scripts are required (much of what should be variable-ised is hard-coded, for now):
1. `cd` into your `run` directory and create symlinks (`ln -s`) to the files `runCondor*`, which are located in the `scripts` directory.
2. In `runCondorSubmission.sh`, change the variables `C_ROOTDIR` (where you have your `source`, `build`, and `run` directories) and `C_RUNDIR` (where you want to call `xAH_run.py` from).
3. In `runCondorSubmission.sub`, change the `MC_TYPE` (`tc16` or `ssm16`) and `RUN#` (directories with the `ROOT` files).
4. `condor_submit runCondorSubmission.sub`

# TODO #
None.

# Current Issues #
None!
