#!/bin/bash
# Script used by runCondorSubmission.sub to submit jobs to the condor batch system

C_THISDIR=`pwd`
C_ROOTDIR=/afs/cern.ch/user/v/vpascuzz/work/private/analysis/lxqq/code/DijetFramework_r21
C_BUILDDIR=$C_ROOTDIR/build
C_RUNDIR=/eos/user/v/vpascuzz/dijetlepton
C_DATASETDIR=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/mc
C_CONFIGFILE=config_mc16a_SignalSyst.py

echo "--cd $C_BUILDDIR"
cd $C_BUILDDIR
echo "--export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo "--source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo "acmSetup"
acmSetup
echo "--cd $C_RUNDIR"
cd $C_RUNDIR
echo "--mkdir -p $1-$2 && cd $1-$2"
mkdir -p $1-$2 && cd $1-$2
echo "--xAH_run.py --files $C_DATASETDIR/$1/$2/* --config $WorkDir_DIR/data/DijetResonanceAlgo/config/$C_CONFIGFILE -f direct"
xAH_run.py --files $C_DATASETDIR/$1/$2/* --config $WorkDir_DIR/data/DijetResonanceAlgo/config/$C_CONFIGFILE -f direct
