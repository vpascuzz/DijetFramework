import ROOT
from xAODAnaHelpers import Config

c = Config()

#%%%%%%%%%%%%%%%%%%%%%%%%%% BasicEventSelection %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("BasicEventSelection",    {
  "m_name"                          : "DijetLepton_BasicEventSelection",
  "m_applyGRLCut"                 : True,
  "m_GRLxml"                    : "GoodRunsLists/data16_13TeV/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
  "m_derivationName"              : "EXOT2",
  "m_useMetaData"                 : False,
  "m_storePassHLT"                : True,
  "m_applyTriggerCut"             : False,
  "m_storeTrigDecisions"          : True,
  "m_triggerSelection"            : "HLT_.*j.*|HLT_.*mu.*|L1_.*J.*|L1_.*MU.*|HLT_.*e.*|L1_.*E.*",
  "m_checkDuplicatesData"         : False,
  "m_applyEventCleaningCut"       : False,
  "m_doPUreweighting"             : True
  } )

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("JetCalibrator",     {
  "m_name"                          : "DijetLepton_JetCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "AntiKt4EMTopoJets",
  "m_jetAlgo"                 :   "AntiKt4EMTopo",
  "m_outContainerName"        :   "AntiKt4EMTopoJets_Calib",
  "m_outputAlgo"              :   "AntiKt4EMTopoJets_Calib_Algo",
  "m_sort"                    :   True,
  "m_redoJVT"                 :   True,
  #----------------------- Systematics ----------------------------#
  "m_systName"                :   "All",                 ## For MC
  #"m_systValVectorString"     :   "0.5,1,1.5,2,2.5,3.0", ## For MC signal samples
  "m_systVal"                 :   1,                    ## For MC background samples
  #"m_systName"                :   "Nominal",            ## For data
  #"m_systVal"                 :   0,                    ## For data
  #----------------------- Calibration ----------------------------#
  "m_calibConfigAFII"         :   "JES_MC15Prerecommendation_AFII_June2015.config",
  "m_calibConfigFullSim"      :   "JES_MC16Recommendation_28Nov2017.config",
  "m_calibConfigData"         :   "JES_MC16Recommendation_28Nov2017.config",
  #JetCalibrator adds _Insitu for the data
  "m_calibSequence"           :   "JetArea_Residual_Origin_EtaJES_GSC",
  #----------------------- JVT ----------------------------#
  "m_JvtAuxName"              :   "JVFCorr",
  #----------------------- JES Uncertainty ----------------------------#
  "m_JESUncertConfig"         :   "$WorkDir_DIR/data/DijetResonanceAlgo/JES2016_SR_Scenario1.config",
 #"m_JESUncertConfig"         :   "$ROOTCOREBIN/data/JetUncertainties/JES_2015/Moriond2016/JES2015_AllNuisanceParameters.config",
  ### For Fullsim ###
  "m_JESUncertMCType"         :   "MC15",
  ### For AFII ###
  #m_JESUncertMCType           :   "AFII",
  #m_setAFII                   :   True,
  #----------------------- JER Uncertainty ----------------------------#
  "m_JERUncertConfig"         :   "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
  "m_JERFullSys"              :   False,
  "m_JERApplyNominal"         :   False,
  #----------------------- Cleaning ----------------------------#
  "m_jetCleanCutLevel"        :   "LooseBad",
  "m_jetCleanUgly"            :   False,
  "m_saveAllCleanDecisions"   :   True,
  "m_cleanParent"             :   False,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                   :   "Info"

  } )

#%%%%%%%%%%%%%%%%%%%%%%%%%% JetSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("JetSelector",     {
  "m_name"                          : "DijetLepton_JetSelector",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "AntiKt4EMTopoJets_Calib",
  "m_outContainerName"        :   "SignalJets",
  "m_inputAlgo"               :   "AntiKt4EMTopoJets_Calib_Algo",
  "m_outputAlgo"              :   "SignalJets_Algo",
  "m_decorateSelectedObjects" :   True,
  "m_createSelectedContainer" :   True,
  #----------------------- Selections ----------------------------#
  ### max/min selections apply to m_pT, m_eta, m_detEta, m_mass, m_rapidity
  "m_cleanJets"               :   False,
  "m_pass_min"                :   2,
  "m_pT_min"                  :   20e3,
  "m_eta_max"                 :   5,       #2.5,
  #----------------------- JVF ----------------------------#
  "m_doJVF"                   :   True,
  #----------------------- JVT ----------------------------#
  "m_doJVT"                   :   True,
  "m_pt_max_JVT"              :   60e3,
  "m_eta_max_JVT"             :   2.4,
  "m_JVTCut"                  :   0.59,
  #----------------------- B-tagging ----------------------------#
  "m_doBTagCut"               :   False,
  #"m_jetAuthor"               :   "AntiKt4EMTopoJets",
  #"m_taggerName"              :   "MV2c20",
  #"m_operatingPt"             :   "FixedCutBEff_70",
  #"m_b_eta_max"               :   2.5,
  #"m_b_pt_min"                :   20e3,
  #----------------------- ??? HLT B-tagging ----------------------------#
  #"m_doHLTBTagCut"            :   False,
  #"m_HLTBTagTaggerName"       :  "MV2c20",
  #"m_HLTBTagCutValue"         :  -0.4434,
  #----------------------- Other ----------------------------#
  ### Require jets pass truth label of (0,1,2,3,21) or 4 or 5
  #"m_truthLabel"              :   5,
  ### Choose the scale at which eta is checked
  #"m_jetScaleType"            :   "JetEMScaleMomentum",
  ### Only check the first N jets
  #"m_nToProcess"              :   2,
  ### Require the first N jets to be clean, otherwise fail event
  #"m_cleanEvtLeadJets"        :   2,
  "m_msgLevel"                   :   "Info"

  } )

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("MuonCalibrator",     {
  "m_name"                          : "DijetLepton_MuonCalib",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "Muons",
  "m_outContainerName"        :   "Muons_Calib",
  #----------------------- Systematics ----------------------------#
  #"m_inputAlgoSystNames"     :   "All",                 ## For MC
  #"m_outputAlgoSystNames"    :   "MuonCalibrator_Syst", ## For MC signal samples
  #"m_systVal"                :   1,                    ## For MC background samples
  "m_systName"                :   "",            ## For data
  "m_systVal"                 :   0,                    ## For data
#  "m_forceDataCalib"          :   0,                    ## For data
  #----------------------- Other ----------------------------#
  "m_sort"                    :    True,
  "m_msgLevel"                   :    "Info"

  } )

#%%%%%%%%%%%%%%%%%%%%%%%%%% MuonSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("MuonSelector",     {
  "m_name"                          : "DijetLepton_MuonSelector",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "Muons_Calib",
  "m_outContainerName"        :   "Muons_Signal",
  "m_createSelectedContainer" :   True,
  #----------------------- Systematics ----------------------------#
  #"m_inputAlgoSystNames"     :   "All",                 ## For MC
  #"m_outputAlgoSystNames"    :   "MuonSelector_Syst", ## For MC signal samples
  #----------------------- configurable cuts ----------------------------#
  "m_muonQualityStr"          : "VeryLoose",
  # "m_muonType"                : "",
  "m_pass_max"                : -1,
  "m_pass_min"                : -1,
  "m_pT_max"                  : 1e8,
  "m_pT_min"                 : 1,
  "m_eta_max"                 : 1e8,
  "m_d0_max"                  : 1e8,
  "m_d0sig_max"     	        : 1e8,
  "m_z0sintheta_max"          : 1e8,

  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"             : "",
  #"m_IsoWPList"		    : "LooseTrackOnly,Loose,Tight,Gradient,GradientLoose",
  #"m_CaloIsoEff"              : "0.1*x+90",
  #"m_TrackIsoEff"             : "98",
  #"m_CaloBasedIsoType"        : "topoetcone20",
  #"m_TrackBasedIsoType"       : "ptvarcone30",

  #----------------------- trigger matching stuff ----------------------------#
  "m_singleMuTrigChains"      : "HLT_mu50",
  #"m_diMuTrigChains"          : "",
  "m_minDeltaR"               : 0.1, # Recommended threshold for muon triggers: see https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigAnalysis/TriggerMatchingTool/trunk/src/TestMatchingToolAlg.cxx
  #"m_doTrigMatch"             : True,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                   :   "Info"

  } )


#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronCalibrator %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("ElectronCalibrator",     {
  "m_name"                          : "DijetLepton_ElectronCalib",
# class in ANAHelper
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "Electrons",
  "m_outContainerName"        :   "Electrons_Calib",
# data member/ variable in class : name of the container in the xAOD file
# meaning the variable/
# m -> passing to another program

  #----------------------- Systematics ----------------------------#
  #"m_inputAlgoSystNames"     :   "All",                 ## For MC
  #"m_outputAlgoSystNames"    :   "ElectronCalibrator_Syst", ## For MC signal samples
  #"m_systVal"                :   1,                    ## For MC background samples
  "m_systName"                :   "Nominal",            ## For data
  "m_systVal"                 :   0,                    ## For data
  "m_esModel"                 :   "es2016PRE",
  "m_decorrelationModel"      :   "1NP_v1",
# make the input the class work
# setting
  #----------------------- Other ----------------------------#
  "m_sort"                    :    True,
  "m_msgLevel"                   :    "Info"

  } )


###########
# which class -> MET
# what variables
# what configuration
# test run
#%%%%%%%%%%%%%%%%%%%%%%%%%% MET %%%%%%%%%%%%%%%%%%%%%%%%%%#
#c.sealg("", {


#}

#)

#%%%%%%%%%%%%%%%%%%%%%%%%%% ElectronSelector %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("ElectronSelector",     {
  "m_name"                          : "DijetLepton_ElectronSelector",
  #----------------------- Container Flow ----------------------------#
  "m_inContainerName"         :   "Electrons_Calib",
  "m_outContainerName"        :   "Electrons_Signal",
  "m_createSelectedContainer" :   True,
  #----------------------- Systematics ----------------------------#
  #"m_inputAlgoSystNames"     :   "All",                 ## For MC
  #"m_outputAlgoSystNames"    :   "ElectronSelector_Syst", ## For MC signal samples
  #----------------------- configurable cuts ----------------------------#
  "m_pass_max"                : -1,
  "m_pass_min"                : -1,
  "m_pT_max"                  : 1e8,
  "m_pT_min"                 : 1,
  "m_eta_max"                 : 1e8,
  "m_d0_max"                  : 1e8,
  "m_d0sig_max"     	        : 1e8,
  "m_z0sintheta_max"          : 1e8,

  #----------------------- isolation stuff ----------------------------#
  "m_MinIsoWPCut"             : "",
  #"m_IsoWPList"		    : "LooseTrackOnly,Loose,Tight,Gradient,GradientLoose",
  #"m_CaloIsoEff"              : "0.1*x+90",
  #"m_TrackIsoEff"             : "98",
  #"m_CaloBasedIsoType"        : "topoetcone20",
  #"m_TrackBasedIsoType"       : "ptvarcone30",

  #----------------------- trigger matching stuff ----------------------------#
  "m_singleElTrigChains"      : "HLT_e60_medium",
  #"m_diMuTrigChains"          : "",
#  "m_minDeltaR"               : 0.07, # Recommended threshold for muon triggers: see https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigAnalysis/TriggerMatchingTool/trunk/src/TestMatchingToolAlg.cxx
  #"m_doTrigMatch"             : True,
  #----------------------- Other ----------------------------#
  "m_msgLevel"                   :   "Info"

  } )

  #%%%%%%%%%%%%%%%%%%%%%%%%%% METConstructor %%%%%%%%%%%%%%%%%%%%%%%%%%#
  # Must be done before overlap removal
c.setalg("METConstructor",     {
  "m_name"                  : "DijetLepton_METConstructor",
  "m_msgLevel"		              : "Info",
  "m_referenceMETContainer" : "MET_Reference_AntiKt4EMTopo",
  "m_mapName"               : "METAssoc_AntiKt4EMTopo",
  "m_coreName"              : "MET_Core_AntiKt4EMTopo",
  "m_outputContainer"       : "MET_rebuilt",
  "m_rebuildUsingTracksInJets"        : False,
  "m_addSoftClusterTerms"       : True,
  "m_inputElectrons"	      : "Electrons_Calib",
  "m_inputMuons"		        : "Muons_Calib",
  "m_inputJets"		          : "AntiKt4EMTopoJets_Calib",
  "m_doJVTCut"	            : True,
  "m_doMuonEloss"           : True,
  "m_doIsolMuonEloss"       : True
} )


#%%%%%%%%%%%%%%%%%%%%%%%%%% BJetEfficiencyCorrector %%%%%%%%%%%%%%%%%%%%%%%%%%#
bJetWPs = [
    "FixedCutBEff_60",
    "FixedCutBEff_70",
    "FixedCutBEff_77",
    "FixedCutBEff_85"
    ]

for bJetWP in bJetWPs:
  c.setalg("BJetEfficiencyCorrector",     {
    #----------------------- Container Flow ----------------------------#
    "m_name"                      :   bJetWP,
    "m_inContainerName"           :   "SignalJets",
    "m_jetAuthor"                 :   "AntiKt4EMTopoJets",
    "m_decor"                     :   "BTag",
    "m_outputSystName"            :   "BJetEfficiency_Algo",
    ### SystName,  All for all, commented if none
    "m_systName"                  :   "All",
    #----------------------- B-tag Options ----------------------------#
    # "m_corrFileName"              :   "$ROOTCOREBIN/data/DijetResonanceAlgo/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root",
    "m_corrFileName"              :   "$WorkDir_DIR/data/DijetResonanceAlgo/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root",
    "m_taggerName"                :   "MV2c10",
    "m_operatingPt"               :   bJetWP,
    "m_coneFlavourLabel"          :   True,
    "m_useDevelopmentFile"        :   True,
    #----------------------- Other ----------------------------#
    "m_msgLevel"                     :   "Info"

    } )

##%%%%%%%%%%%%%%%%%%%%%%%%%% DijetResonanceAlgo %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.setalg("ResonanceAlgorithm",     {
  "m_name"                        : "DijetResonanceAlgo",
    #----------------------- Container Flow ----------------------------#
    "m_inContainerName"           :   "SignalJets",
    "m_inMuContainerName"         :   "Muons_Signal",
    "m_inElContainerName"         :   "Electrons_Signal",
    "m_inMETContainerName"	      :   "MET_rebuilt",
    "m_inputAlgo"                 :   "SignalJets_Algo",
    "m_allJetContainerName"       :   "AntiKt4EMTopoJets_Calib",
    "m_allJetInputAlgo"           :   "AntiKt4EMTopoJets_Calib_Algo",
    #----------------------- Selections ----------------------------#
    "m_leadingJetPtCut"           :   0000,
#    "m_yStarCut"                  :   2.0,
    #----------------------- Output ----------------------------#
    "m_doBtag"                    :   True,
    "m_bTagWPNames"               :   "FixedCutBEff_60,FixedCutBEff_70,FixedCutBEff_77,FixedCutBEff_85",
    "m_eventDetailStr"            :   "truth pileup shapeEM",
    "m_jetDetailStr"              :   "kinematic rapidity clean energy truth truth_details flavorTag trackPV trackAll allTrack allTrackPVSel allTrackDetail allTrackDetailPVSel btag_jettrk",
    "m_jetDetailStrSyst"          :   "kinematic rapidity clean energy truth truth_details flavorTag trackPV trackAll allTrack allTrackPVSel allTrackDetail allTrackDetailPVSel btag_jettrk",
    "m_elDetailStr"               :   "kinematic trigger isolation PID trackparams trackhitcont effSF",
    "m_muDetailStr"               :   "kinematic trigger isolation quality trackparams trackhitcont effSF energyLoss",
    "m_trigDetailStr"             :   "passTriggers",
    #----------------------- Other ----------------------------#
    "m_writeTree"                 :   True,
    "m_MCPileupCheckContainer"    :   "AntiKt4TruthJets",
#    "m_truthLevelOnly"            :   False,
    "m_msgLevel"                     :   "Info"

    } )
